package itis.socialtest;

import itis.socialtest.entities.Post;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;

public class AnalyticsServiceImpl implements AnalyticsService {
    @Override
    public List<Post> findPostsByDate(List<Post> posts, String date) {
        return posts.stream()
                .filter(post -> post.getDate().equals(date))
                .collect(Collectors.toList());
    }

    @Override
    public String findMostPopularAuthorNickname(List<Post> posts) throws IOException {
        FileInputStream fileInputStream = new FileInputStream("Authors.cvs");
        long i;
        long count = 0;
        while ((i = fileInputStream.read()) != -1) {
            count++;
        }
        long[] arr = new long[(int) count];
        for (Post post : posts) {
            arr[(int) (post.getAuthor().getId() - 1)] += post.getLikesCount();
        }
        long max = -1;
        long ans = -2;
        for (int j = 0; j < count; j++) {
            if (max < arr[j]) {
                max = arr[j];
                ans = j + 1;
            }
        }
        for (Post post : posts) {
            if (post.getAuthor().getId() == ans) {
                return post.getAuthor().getNickname();
            }
        }
        return null;
    }

    @Override
    public Boolean checkPostsThatContainsSearchString(List<Post> posts, String searchString) {
        return posts.stream()
                .anyMatch(post -> post.getContent().equals(searchString));
    }

    @Override
    public List<Post> findAllPostsByAuthorNickname(List<Post> posts, String nick){
        return posts.stream()
                .filter(post -> post.getAuthor().getNickname().equals(nick))
                .collect(Collectors.toList());    }
}
